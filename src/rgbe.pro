#-------------------------------------------------
#
# Project created by QtCreator 2012-03-29T10:13:18
#
#-------------------------------------------------

QT       -= core gui

TARGET = rgbe
TEMPLATE = lib
CONFIG += staticlib

SOURCES += rgbe.cpp

HEADERS += rgbe.h
unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
